package INF101.lab2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
	
	private List<FridgeItem> fridgeContent = new ArrayList<FridgeItem>();
	

	@Override
	public int nItemsInFridge() {
		return fridgeContent.size();
	}

	
	@Override
	public int totalSize() {
		int spaceInFridge = 20;
		return spaceInFridge;
	}

	
	@Override
	public boolean placeIn(FridgeItem item) {
		
		int numberOfItems = nItemsInFridge();
		int spaceInFridge = totalSize();
		
		if (numberOfItems < spaceInFridge) {
			fridgeContent.add(item);
			return true;
		} else {
			return false;
		}
	}

	
	@Override
	public void takeOut(FridgeItem item) {
		
		if (fridgeContent.contains(item)) {
			fridgeContent.remove(item);
		} else {
			throw new NoSuchElementException("Fridge does not contain item."); 
		}
	}

	
	@Override
	public void emptyFridge() {
		fridgeContent.clear();
	}

	
	@Override
	public List<FridgeItem> removeExpiredFood() {
		
		ArrayList<FridgeItem> expiredFood = new ArrayList<FridgeItem>();
		
		for (FridgeItem item : fridgeContent) {
			if (item.hasExpired()) {
				expiredFood.add(item);
			}
		}
		
		fridgeContent.removeAll(expiredFood);
		return expiredFood;
	}

}
